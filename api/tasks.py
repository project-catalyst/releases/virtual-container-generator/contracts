from celery.task import task

from api.exceptions import OperationTimeout
from api.utils import transfer_ether_amount, wait_for_transaction_mined


@task
def transfer_ether_task(sender, recipient, amount):
    """Blocking task that transfers ether to the recipient.

    Args:
        recipient: hexstring address of recipient
        amount (int): the ether amount

    Returns:
        True if the transfer was successfully, False otherwise
    """
    tx_hash = transfer_ether_amount(sender, recipient, amount)

    # blocks for 60 seconds max (15 seconds on average)
    try:
        wait_for_transaction_mined(tx_hash, interval=1000, max_retries=60)
        return True
    except OperationTimeout:
        return False
