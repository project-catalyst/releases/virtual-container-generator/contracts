import logging
import uuid
from time import sleep

from django.conf import settings
from eth_utils import to_wei
from web3 import Web3, HTTPProvider

from api.exceptions import MessageLogTimeout, OperationTimeout, VMException
from ws.ethereum import CatalystEthereumClient
from ws.exceptions import AccountLocked
from ws.models import DatacenterMessageLog


log = logging.getLogger(__name__)


def poll_for_result(request_id, interval=50, max_retries=100):
    """Polls the database until a response has been received for the given request_id

    Args:
        request_id (str): the request id uuid
        interval: sleep duration ( in millis )
        max_retries: times to retry before raising exception

    Returns:
        the JSONField dict response payload.

    Raises:
        MessageLogTimeout: when the dc did not respond in time
        VMException: when the underlying dc ethereum client raised an exception
    """
    retries = 0
    request_id = uuid.UUID(request_id)

    while retries < max_retries:
        log.debug('Polling for request_id={} retry={}'.format(str(request_id), retries))
        message_log = DatacenterMessageLog.objects.filter(
            request_id=request_id, response_payload__isnull=False
        )

        if message_log.exists():
            message_log = message_log.first()

            # If the underlying ethereum client raised an exception
            if message_log.evm_exception:
                raise VMException
            return message_log.response_payload

        sleep(interval / 1000.0)
        retries += 1

    raise MessageLogTimeout


def wait_for_transaction_mined(tx_hash, interval=1000, max_retries=600):
    """Waits for the given transaction to be mined.

    Args:
        tx_hash: the transaction hash
        interval: the polling interval
        max_retries: maximum retries before raising Exception

    Returns:
        dict: the mined transaction details
    """
    retries = 0
    client = CatalystEthereumClient()

    while retries < max_retries:
        log.debug('Waiting for tx_hash={} to be mined, retry={}'.format(str(tx_hash), retries))
        result = client.web3.eth.getTransaction(tx_hash)

        if result:
            return result

        sleep(interval / 1000.0)
        retries += 1

    raise OperationTimeout


def transfer_ether_amount(sender, recipient, amount):
    """Creates and broadcasts a transaction transferring ether from the server wallet address to the
    targeted recipient.

    Args:
        recipient: hexstring address of the recipient
        amount (int): the ether amount

    Returns:
        string: the transaction hash
    """
    client = CatalystEthereumClient()
    tx_hash = None

    with client.account_unlocked() as unlocked:
        if unlocked:
            tx_hash = client.web3.eth.sendTransaction(
                {'from': sender, 'to': recipient, 'value': to_wei(amount, 'ether')})
        else:
            raise AccountLocked("Eth account is locked, verify that you provided the correct credentials")

    return tx_hash
