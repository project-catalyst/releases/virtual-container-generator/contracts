from django.conf.urls import url
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from . import views

API_DESCRIPTION = """Api endpoints exposed by the Catalyst VCG API Server.

The `swagger-ui` view can be found [here](/api/swagger).  
The `ReDoc` view can be found [here](/api/redoc).  
The swagger YAML document can be found [here](/api/swagger.yaml).  
"""

schema_view = get_schema_view(
   openapi.Info(
      title="VCG API",
      default_version='v1',
      description=API_DESCRIPTION,
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="tomaras@synelixis.com"),
      #license=openapi.License(name="BSD License"),
   ),
   validators=[],
   public=True,
   permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),


    url(r'^datacenter/register/$', views.RegisterDatacenter.as_view()),
    url(r'^datacenter/(?P<address>[A-Za-z0-9]+)/details/$', views.DatacenterDetails.as_view()),
    url(r'^datacenter/(?P<dc_name>[A-Za-z0-9_]+)/container/register/$', views.RegisterContainer.as_view()),
    url(r'^datacenter/(?P<dc_name>[A-Za-z0-9_]+)/container/(?P<vc_tag>[A-Za-z0-9]+)/migrate/pending/$',
        views.PendingMigrateContainer.as_view()),
    url(r'^datacenter/(?P<dc_name>[A-Za-z0-9_]+)/container/(?P<vc_tag>[A-Za-z0-9]+)/availability/change/$',
        views.ChangeContainerAvailability.as_view()),
    url(r'^datacenter/(?P<dc_name>[A-Za-z0-9_]+)/container/(?P<vc_tag>[A-Za-z0-9]+)/migrate/confirm/$',
        views.ConfirmMigrateContainer.as_view()),
    url(r'^container/(?P<vc_tag>[A-Za-z0-9]+)/details/$', views.ContainerDetails.as_view()),
    url(r'^container/(?P<vc_tag>[A-Za-z0-9]+)/flavor/$', views.ContainerFlavorDetails.as_view()),
    url(r'^container/(?P<vc_tag>[A-Za-z0-9]+)/history/$', views.ContainerHistory.as_view()),
    url(r'^container/(?P<vc_tag>[A-Za-z0-9]+)/migration/pending/$', views.ContainerPendingMigrationDetails.as_view()),
    url(r'^transaction/(?P<tx_hash>[A-Za-z0-9]+)/vctag/$', views.TransactionToVCTag.as_view())
]
