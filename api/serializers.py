import pytz
from eth_utils import is_hex_address
from rest_framework import serializers
from rest_framework.status import *

from ws.models import ChainEvent, DatacenterMessageLog


class ContainerSerializer(serializers.Serializer):
    vc_type = serializers.CharField(max_length=120)
    available = serializers.BooleanField()
    id = serializers.CharField(help_text='Openstack or docker id')
    ip_address = serializers.URLField(help_text='Valid ip address, e.g: `http://127.0.0.1`')
    controller = serializers.URLField(help_text='Valid ip address, e.g: `http://127.0.0.1`')
    host = serializers.CharField(max_length=42, min_length=42, help_text='Wallet public key of the host DC')
    owner = serializers.CharField(max_length=42, min_length=42, help_text='Wallet public key of the owner DC')
    updated = serializers.DateTimeField()
    created = serializers.DateTimeField()


class ContainerFlavorSerializer(serializers.Serializer):
    vcpu = serializers.IntegerField()
    vram = serializers.IntegerField()
    vdisk = serializers.IntegerField()


class PendingMigrationContainerSerializer(serializers.Serializer):
    vc_tag = serializers.CharField(max_length=66, min_length=66)
    from_address = serializers.CharField(max_length=42, min_length=42)
    to_address = serializers.CharField(max_length=42, min_length=42)
    price = serializers.IntegerField()
    invoice = serializers.IntegerField()


class DatacenterSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=120)
    wallet = serializers.CharField(max_length=42, min_length=42, help_text='Wallet public key of the DC')
    registered = serializers.BooleanField()


class RegisterDatacenterSerializer(serializers.Serializer):
    """Values to pass inside the blockchain smart contract.

    create: hello there
    """
    name = serializers.CharField(max_length=120)
    wallet = serializers.CharField(max_length=42, min_length=42, help_text='Wallet public key of the DC')

    def validate_wallet(self, wallet):
        if not is_hex_address(wallet):
            raise serializers.ValidationError('Wallet address is not a valid hex address')
        return wallet


class ChainEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChainEvent
        fields = serializers.ALL_FIELDS


class TransactionToVcTagSerializer(serializers.ModelSerializer):
    vc_tag = serializers.SerializerMethodField(help_text='The vcontainer vcTag that the transaction generated')

    class Meta:
        model = ChainEvent
        fields = ['vc_tag', 'block_number', 'block_hash', 'created']

    def get_vc_tag(self, obj):
        return obj.args['vcTag']


class RegisterContainerSerializer(serializers.Serializer):
    vc_type = serializers.CharField(max_length=120)
    available = serializers.BooleanField()
    vcpu = serializers.IntegerField(min_value=1)
    vram = serializers.IntegerField(min_value=1)
    vdisk = serializers.IntegerField(min_value=1)
    id = serializers.CharField(max_length=120)
    ip_address = serializers.URLField(help_text='Valid ip address, e.g: `http://127.0.0.1`')
    controller = serializers.URLField(help_text='Valid ip address, e.g: `http://127.0.0.1`')
    created = serializers.DateTimeField(default_timezone=pytz.UTC)


class PendingMigrateContainerSerializer(serializers.Serializer):
    target = serializers.CharField(max_length=42, min_length=42)
    price = serializers.IntegerField(min_value=0)
    invoice = serializers.IntegerField(min_value=1)
    timestamp = serializers.DateTimeField(default_timezone=pytz.UTC)

    def validate_target(self, target):
        if not is_hex_address(target):
            raise serializers.ValidationError('Target address is not a valid hex address')
        return target


class ConfirmMigrateContainerSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=120)
    ip_address = serializers.URLField(help_text='Valid ip address, e.g: `http://127.0.0.1`')
    controller = serializers.URLField(help_text='Valid ip address, e.g: `http://127.0.0.1`')
    price = serializers.IntegerField(min_value=0)
    invoice = serializers.IntegerField(min_value=1)
    timestamp = serializers.DateTimeField(default_timezone=pytz.UTC)


class ChangeContainerAvailabilitySerializer(serializers.Serializer):
    status = serializers.BooleanField()
    timestamp = serializers.DateTimeField(default_timezone=pytz.UTC)


class TransactionHashSerializer(serializers.Serializer):
    """The transaction hash.
    """
    tx_hash = serializers.CharField(max_length=66, min_length=66)