"""
WSGI config for vcg_api_server project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os
import sys

import djcelery
from django.core.wsgi import get_wsgi_application

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(BASE_DIR, '..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "vcg_api_server.settings.prod")

application = get_wsgi_application()
djcelery.setup_loader()
