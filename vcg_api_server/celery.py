import os
from celery import Celery
from django.conf import settings

from vcg_api_server.settings import get_env_setting

settings_file = get_env_setting('DJANGO_SETTINGS_MODULE')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', settings)
app = Celery('vcg_api_server')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
