def get_event_abi(abi, event_name):
    """Helper function that extracts the event abi from the given abi dict object.
    """
    for entry in abi:
        if 'name' in entry.keys() and entry['name'] == event_name and entry['type'] == "event":
            return entry
    raise ValueError('Event with name `{}` not found in the contract abi'.format(event_name))


CONTRACT_ABI_RAW = """
[
    {
      "constant": true,
      "inputs": [
        {
          "name": "",
          "type": "bytes32"
        }
      ],
      "name": "pendingMigrations",
      "outputs": [
        {
          "name": "vcTag",
          "type": "bytes32"
        },
        {
          "name": "from",
          "type": "address"
        },
        {
          "name": "to",
          "type": "address"
        },
        {
          "name": "price",
          "type": "uint256"
        },
        {
          "name": "invoice",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "name": "datacenters",
      "outputs": [
        {
          "name": "name",
          "type": "string"
        },
        {
          "name": "wallet",
          "type": "address"
        },
        {
          "name": "registered",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "",
          "type": "bytes32"
        }
      ],
      "name": "containers",
      "outputs": [
        {
          "name": "vcTag",
          "type": "bytes32"
        },
        {
          "name": "vcType",
          "type": "string"
        },
        {
          "name": "available",
          "type": "bool"
        },
        {
          "components": [
            {
              "name": "vcpu",
              "type": "uint8"
            },
            {
              "name": "vram",
              "type": "uint8"
            },
            {
              "name": "vdisk",
              "type": "uint8"
            }
          ],
          "name": "flavor",
          "type": "tuple"
        },
        {
          "components": [
            {
              "name": "id",
              "type": "string"
            },
            {
              "name": "ipAddress",
              "type": "string"
            },
            {
              "name": "host",
              "type": "address"
            },
            {
              "name": "controller",
              "type": "string"
            }
          ],
          "name": "info",
          "type": "tuple"
        },
        {
          "name": "owner",
          "type": "address"
        },
        {
          "name": "created",
          "type": "uint256"
        },
        {
          "name": "updated",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "wallet",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "timestamp",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "name",
          "type": "string"
        }
      ],
      "name": "DatacenterRegistered",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "vcTag",
          "type": "bytes32"
        },
        {
          "indexed": false,
          "name": "timestamp",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "owner",
          "type": "address"
        }
      ],
      "name": "ContainerRegistered",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "vcTag",
          "type": "bytes32"
        },
        {
          "indexed": false,
          "name": "timestamp",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "oldStatus",
          "type": "bool"
        },
        {
          "indexed": false,
          "name": "newStatus",
          "type": "bool"
        }
      ],
      "name": "ContainerAvailabilityChanged",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "vcTag",
          "type": "bytes32"
        },
        {
          "indexed": false,
          "name": "timestamp",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "from",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "to",
          "type": "address"
        }
      ],
      "name": "ContainerMigrationPending",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "vcTag",
          "type": "bytes32"
        },
        {
          "indexed": false,
          "name": "timestamp",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "from",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "to",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "price",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "invoice",
          "type": "uint256"
        }
      ],
      "name": "ContainerMigrated",
      "type": "event"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "vcTag",
          "type": "bytes32"
        }
      ],
      "name": "getContainer",
      "outputs": [
        {
          "name": "vcType",
          "type": "string"
        },
        {
          "name": "available",
          "type": "bool"
        },
        {
          "name": "id",
          "type": "string"
        },
        {
          "name": "ipAddress",
          "type": "string"
        },
        {
          "name": "host",
          "type": "address"
        },
        {
          "name": "controller",
          "type": "string"
        },
        {
          "name": "owner",
          "type": "address"
        },
        {
          "name": "created",
          "type": "uint256"
        },
        {
          "name": "updated",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "vcTag",
          "type": "bytes32"
        }
      ],
      "name": "getContainerFlavor",
      "outputs": [
        {
          "name": "vcpu",
          "type": "uint8"
        },
        {
          "name": "vram",
          "type": "uint8"
        },
        {
          "name": "vdisk",
          "type": "uint8"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "name",
          "type": "string"
        },
        {
          "name": "wallet",
          "type": "address"
        }
      ],
      "name": "registerDatacenter",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "vcType",
          "type": "string"
        },
        {
          "name": "available",
          "type": "bool"
        },
        {
          "name": "vcpu",
          "type": "uint8"
        },
        {
          "name": "vram",
          "type": "uint8"
        },
        {
          "name": "vdisk",
          "type": "uint8"
        },
        {
          "name": "id",
          "type": "string"
        },
        {
          "name": "ipAddress",
          "type": "string"
        },
        {
          "name": "controller",
          "type": "string"
        },
        {
          "name": "created",
          "type": "uint256"
        }
      ],
      "name": "registerContainer",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "vcTag",
          "type": "bytes32"
        },
        {
          "name": "timestamp",
          "type": "uint256"
        },
        {
          "name": "target",
          "type": "address"
        },
        {
          "name": "price",
          "type": "uint256"
        },
        {
          "name": "invoice",
          "type": "uint256"
        }
      ],
      "name": "pendingMigrate",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "vcTag",
          "type": "bytes32"
        },
        {
          "name": "price",
          "type": "uint256"
        },
        {
          "name": "invoice",
          "type": "uint256"
        },
        {
          "name": "id",
          "type": "string"
        },
        {
          "name": "ipAddress",
          "type": "string"
        },
        {
          "name": "controller",
          "type": "string"
        },
        {
          "name": "timestamp",
          "type": "uint256"
        }
      ],
      "name": "migrateContainer",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "vcTag",
          "type": "bytes32"
        },
        {
          "name": "status",
          "type": "bool"
        },
        {
          "name": "timestamp",
          "type": "uint256"
        }
      ],
      "name": "changeAvailability",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    }
  ]
"""