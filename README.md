# Catalyst Ethereum
This repository holds the Ethereum smart contracts related with Catalyst.

### Required software
The following things need to be installed:
1.  Node.js, follow these [instructions](https://nodejs.org/en/download/)
2.  npm, should be installed when installing node.js

Before proceeding verify the following commands work:

```
node -v
npm -v
```

### Creating a local development environment
Install the dependencies:

```
npm install -g truffle ganache-cli
npm install --save truffle-assertions
```

Clone the repo and open two cmd consoles, if the first execute:
```
ganache-cli -d -p 7545
```

on the second;
```
truffle compile
truffle migrate
```

To run the unit tests do:
```
truffle test
```

## Deploying to a geth/parity remote node
To connect to a geth/parity node, open the `truffle.js` and edit the values
for the `staging` network entry to match with those of your remote node.

To deploy the smart contract and apply migrations execute:

```
truffle migrate --network staging
```

If the remote wallet address is locked, you can unlock it by using:

```
ACCOUNT_PASSWORD=secretpass truffle migrate --network staging
```

It is imperative for the remote node to have **rpc** enabled and to be able to accept remote connections from the host machine
