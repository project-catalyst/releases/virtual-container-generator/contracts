import math

import pytz
from datetime import datetime

from django.conf import settings
from django_ethereum_events.chainevents import AbstractEventReceiver
from eth_utils import encode_hex

from api.tasks import transfer_ether_task
from ws.models import DatacenterClient
from ws.utils import create_chain_event


class DatacenterRegisteredEventReceiver(AbstractEventReceiver):
    """Fired when a `DatacenterRegistered` is logged from the blockchain.
    """
    def save(self, decoded_event):
        args = decoded_event.pop('args')

        # Preprocess the arguments
        args['wallet'] = args['wallet'].lower()
        args['timestamp'] = datetime.fromtimestamp(args['timestamp'], pytz.UTC)

        # Create the event instance
        create_chain_event(args, decoded_event)

        # Create the datacenter client entry
        client = DatacenterClient.objects.create(address=args['wallet'], name=args['name'])

        # Send the DC address some ether
        transfer_ether_task.delay(settings.WALLET_ADDRESS, client.address, math.pow(10, 5))


class ContainerRegisteredEventReceiver(AbstractEventReceiver):
    """Fired when a `ContainerRegistered` is logged from the blockchain.
    """
    def save(self, decoded_event):
        args = decoded_event['args']

        # Preprocess the arguments
        args['owner'] = args['owner'].lower()
        args['timestamp'] = datetime.fromtimestamp(args['timestamp'], pytz.UTC)
        args['vcTag'] = encode_hex(args['vcTag'])

        # Create the event instance
        create_chain_event(args, decoded_event)


class ContainerAvailabilityChangedEventReceiver(AbstractEventReceiver):
    """Fired when a `ontainerAvailabilityChanged` is logged from the blockchain.
    """
    def save(self, decoded_event):
        args = decoded_event['args']

        # Preprocess the arguments
        args['timestamp'] = datetime.fromtimestamp(args['timestamp'], pytz.UTC)
        args['vcTag'] = encode_hex(args['vcTag'])

        # Create the event instance
        create_chain_event(args, decoded_event)


class ContainerMigrationPendingEventReceiver(AbstractEventReceiver):
    """Fired when a `ContainerMigrationPending` is logged from the blockchain.
    """
    def save(self, decoded_event):
        args = decoded_event['args']

        # Preprocess the arguments
        args['from'] = args['from'].lower()
        args['to'] = args['to'].lower()
        args['timestamp'] = datetime.fromtimestamp(args['timestamp'], pytz.UTC)
        args['vcTag'] = encode_hex(args['vcTag'])

        # Create the event instance
        create_chain_event(args, decoded_event)


class ContainerMigratedEventReceiver(AbstractEventReceiver):
    """Fired when a `ContainerMigrated` is logged from the blockchain.
    """
    def save(self, decoded_event):
        args = decoded_event['args']

        # Preprocess the arguments
        args['from'] = args['from'].lower()
        args['to'] = args['to'].lower()
        args['timestamp'] = datetime.fromtimestamp(args['timestamp'], pytz.UTC)
        args['vcTag'] = encode_hex(args['vcTag'])

        # Create the event instance
        create_chain_event(args, decoded_event)