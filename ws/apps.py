from django.apps import AppConfig


class WsConfig(AppConfig):
    name = 'ws'

    def ready(self):
        super(WsConfig, self).ready()
