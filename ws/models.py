import uuid

from django.db import models
from django.contrib.postgres.fields import JSONField
from django.core.serializers.json import DjangoJSONEncoder


class ChainEvent(models.Model):
    """Blockchain events that are monitored from the given events in the settings.

    When searching based on the tx_hash, block_hash and address it is required to use the
    __iexact queryset to perform case insensitive search.
    """
    name = models.CharField(max_length=120, help_text='Event name')
    args = JSONField(encoder=DjangoJSONEncoder, help_text='Event arguments')
    log_index = models.PositiveIntegerField(help_text='Position of the event in the logs')
    tx_index = models.PositiveIntegerField(help_text='Index of the transaction inside the block')
    tx_hash = models.CharField(max_length=66, help_text='Transaction hash')
    block_number = models.PositiveIntegerField(help_text='Block number')
    block_hash = models.CharField(max_length=66, help_text='Block hash')
    address = models.CharField(max_length=42, help_text='Contract address that the event originated from')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'chain_events'
        verbose_name = 'Blockchain event'
        verbose_name_plural = 'Blockchain events'

    def __str__(self):
        return '{} block={}, tx_hash=...{}'.format(self.name, self.block_number, self.tx_hash[-4:])


class DatacenterClient(models.Model):
    DC_STATUS = (
        ('connected', 'Connected'),
        ('disconnected', 'Disconnected'),
        ('unknown', 'Unknown')
    )
    name = models.CharField(max_length=120, unique=True)
    channel_name = models.CharField(max_length=120, blank=True, null=True, help_text='Ephemeral channel name')
    status = models.CharField(max_length=40, choices=DC_STATUS, default='unknown')
    address = models.CharField(max_length=42, unique=True, help_text='Wallet address of the DC')
    last_connected = models.DateTimeField(blank=True, null=True, help_text='Last timestamp of ws connect')
    last_disconnected = models.DateTimeField(blank=True, null=True, help_text='Last timestamp of ws disconnect')
    last_message = models.DateTimeField(blank=True, null=True, help_text='Last timestamp of message receive')

    class Meta:
        db_table = 'datacenter_clients'
        verbose_name = 'Datacenter client'
        verbose_name_plural = 'Datacenter clients'

    def __str__(self):
        return '{} - {}'.format(self.name, self.status)

    @property
    def group_name(self):
        return 'dc_{}'.format(self.name)


class DatacenterMessageLog(models.Model):
    request_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    dc_client = models.ForeignKey(DatacenterClient, related_name='message_logs', on_delete=models.CASCADE)
    request_payload = JSONField(encoder=DjangoJSONEncoder, help_text='Payload send to the DC ws client')
    response_payload = JSONField(encoder=DjangoJSONEncoder, blank=True, null=True,
                                 help_text='Payload received from the DC ws client')
    request_timestamp = models.DateTimeField(auto_now_add=True, help_text='When the message was sent')
    response_timestamp = models.DateTimeField(blank=True, null=True, help_text='When the message response was received')
    evm_exception = models.BooleanField(default=False, help_text='If the ethereum client threw an exception')

    class Meta:
        db_table = 'datacenter_message_logs'
        verbose_name = 'Datacenter message log'
        verbose_name_plural = 'Datacenter message logs'

    def __str__(self):
        return '{} @ {}'.format(str(self.request_id), self.dc_client.name)


