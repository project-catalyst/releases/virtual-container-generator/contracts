from ws.models import ChainEvent


def create_chain_event(event_args, decoded_event):
    """Creates a ChainEvent object

    Args:
        event_args (dict): the preprocessed event arguments
        decoded_event (dict): the rest event details

    """
    ChainEvent.objects.create(
        name=decoded_event['event'],
        args=event_args,
        log_index=decoded_event['logIndex'],
        tx_index=decoded_event['transactionIndex'],
        tx_hash=decoded_event['transactionHash'].lower(),
        block_hash=decoded_event['blockHash'].lower(),
        block_number=decoded_event['blockNumber'],
        address=decoded_event['address'].lower()
    )