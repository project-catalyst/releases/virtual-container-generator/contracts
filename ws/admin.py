from django.contrib import admin
from django.conf.locale.en import formats as en_formats

from ws import models


en_formats.DATETIME_FORMAT = "Y-m-d H:i:s.u"


class ChainEventAdmin(admin.ModelAdmin):
    list_display = ['name', 'block_number', 'log_index', 'tx_index', 'tx_hash', 'address']
    list_filter = ['block_number']
    ordering = ['address']
    search_fields = ['block_number']


admin.site.register(models.ChainEvent, ChainEventAdmin)


class DatacenterClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'address', 'status', 'channel_name', 'last_connected', 'last_disconnected', 'last_message']
    list_filter = ['name', 'last_message']
    ordering = ['last_message']


admin.site.register(models.DatacenterClient, DatacenterClientAdmin)


class DatacenterMessageLogAdmin(admin.ModelAdmin):
    list_display = ['request_id', 'dc_client', 'request_timestamp', 'response_timestamp', 'evm_exception']
    list_filter = ['dc_client', 'evm_exception']
    ordering = ['request_timestamp']



admin.site.register(models.DatacenterMessageLog, DatacenterMessageLogAdmin)