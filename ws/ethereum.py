from contextlib import contextmanager

from web3 import HTTPProvider, Web3

try:
    from django.conf import settings
except ImportError:
    import settings

from .exceptions import InvalidMessageType, AccountLocked


class CatalystEthereumClient:
    """Connection between the python world and the underlying ethereum blockchain.

    This class handles the web3 interface and exposes all the endpoints provided
    by Catalyst smart contract.
    """
    def __init__(self, *args, **kwargs):
        self.wallet_address = settings.WALLET_ADDRESS
        self.__wallet_password = settings.WALLET_PASSWORD
        self.contract_address = settings.CONTRACT_ADDRESS
        self.contract_abi = settings.CONTRACT_ABI

        if not self.wallet_address or not self.__wallet_password or not self.contract_address  \
                or not self.contract_abi:
            raise ValueError('Ethereum parameters cannot have empty values')

        self.web3 = Web3(HTTPProvider('http://{}:{}'.format(settings.ETHEREUM_HOST, settings.ETHEREUM_PORT)))
        self.contract = self.web3.eth.contract(self.contract_address, abi=self.contract_abi)

    @contextmanager
    def account_unlocked(self):
        """Account locking/unlocking."""
        try:
            yield self.web3.personal.unlockAccount(self.wallet_address, self.__wallet_password)
        finally:
            self.web3.personal.lockAccount(self.wallet_address)

    def handle_message(self, message):
        """Handles an incoming message from the server.

        Args:
            message: dict that contains the necessary input arguments of the contract method
                matching the message['method'] name.

        Returns:
            either a transaction hash, or a result of a contract `call` method invocation.
        """
        message_type = message.pop('method', None)
        if not message_type or not hasattr(self, message_type):
            raise InvalidMessageType('Receive invalid message of type {}'.format(message_type))
        message_handler = getattr(self, message_type)
        result = None
        with self.account_unlocked() as unlocked:
            if unlocked:
                result = message_handler(**message)
            else:
                raise AccountLocked("Eth account is locked, verify that you provided the correct credentials")
        return result

    def get_container(self, **kwargs):
        """Retrieves the vcontainer details with the given vc_tag.

        Args:
            **kwargs: dict with the following entries:
                {
                    vc_tag: hexstring representation of the vc_tag
                }

        Returns:
            list: return arguments of the underlying `getContainer` contract method:
                [
                    vc_type ~ string, the vcontainer type,
                    available ~ boolean, the current availability,
                    id ~ string, the openstack/docker id,
                    ip ~ string, the ip of the container,
                    host ~ string, the wallet address of the current host of the container,
                    controller ~ string, the endpoint of the virtualization controller,
                    created ~ int, unix timestamp of the creation datetime of the container
                    updated ~ int, unix timestamp of the last container update
                ]

        """
        vc_tag = kwargs.pop('vc_tag')
        value = self.contract.call({'from': self.wallet_address}).getContainer(Web3.toBytes(hexstr=vc_tag))
        return value

    def get_container_flavor(self, **kwargs):
        """Retrieves the vcontainer flavor details with the given vc_tag.

        Args:
            **kwargs: dict with the following entries:
                {
                    vc_tag: hexstring representation of the vc_tag
                }

        Returns:
            list: return arguments of the underlying `getContainer` contract method:
                [
                    vcpu ~ int, the vcpu count of the container
                    vram ~ int, the ram amount of the container
                    vdisk ~ int, the disk volume of the container
                ]

        """
        vc_tag = kwargs.pop('vc_tag')
        value = self.contract.call({'from': self.wallet_address}).getContainerFlavor(Web3.toBytes(hexstr=vc_tag))
        return value

    def get_datacenter(self, **kwargs):
        """Retrieve the DC details from the given wallet address.

        Args:
            **kwargs: dict with the following entries:
                {
                    address: string account address
                }

        Returns:
            list: parameters of the `datacenters` getter method:
                [
                    name ~ string, the name of the DC,
                    wallet ~ string, wallet address of the DC ( same as passed argument )
                    registered ~ boolean, if the DC is registered or not
                ]

        """
        address = kwargs.pop('address')
        value = self.contract.call({'from': self.wallet_address}).datacenters(address)
        return value

    def get_pending_migration(self, **kwargs):
        """Retrieve the pending migration details for the vcontainer with the given vc_tag.

        Args:
            **kwargs: dict with the following entries:
                {
                    vc_tag: the vcTag of the relevant vcontainer
                }

        Returns:
            list: parameters of the `pendingMigrations` getter method:
                [
                    vc_tag ~ string, the vcTag of the vcontainer
                    from ~ string, the wallet address of the current host
                    to ~ string, the wallet address of the targeted host
                    price ~ int, the price of the pending migration
                    invoice ~ int, the relevant invoice id
                ]

        """
        vc_tag = kwargs.pop('vc_tag')
        value = self.contract.call({'from': self.wallet_address}).pendingMigrations(Web3.toBytes(hexstr=vc_tag))
        return value

    def register_datacenter(self, **kwargs):
        """Register a new datacenter inside the smart contract. Invoked method is `registerDatacenter`.

        Args:
            **kwargs: dict with the following entries:
                {
                    name: string name of the dc
                    wallet: wallet address (account) of the dc
                }

        Returns:
            string: transaction hash (tx hash)
        """
        name = kwargs.pop('name')
        wallet = kwargs.pop('wallet')

        tx_hash = self.contract.transact({'from': self.wallet_address}).registerDatacenter(
            name=name, wallet=wallet
        )

        return tx_hash

    def register_container(self, **kwargs):
        """Registers a new vcontainer inside the smart contract. Invoked method is `registerContainer`.

        Args:
            **kwargs: dict with the following entries:
                {
                    vc_type: string, the vcontainer type,
                    available: boolean, the current availability,
                    vcpu: int, the vcpu count,
                    vram: int, the vram count,
                    vdisk: int, the vdisk storage capacity,
                    id ~ string, the openstack/docker id,
                    ip_address ~ string, the ip of the container,
                    controller ~ string, the endpoint of the virtualization controller,
                    created ~ int, unix timestamp of the creation datetime of the container
                }

        Returns:
            string: transaction hash (tx hash)
        """
        vc_type = kwargs.pop('vc_type')
        available = kwargs.pop('available')
        vcpu = kwargs.pop('vcpu')
        vram = kwargs.pop('vram')
        vdisk = kwargs.pop('vdisk')
        id = kwargs.pop('id')
        ip_address = kwargs.pop('ip_address')
        controller = kwargs.pop('controller')
        created = kwargs.pop('created')

        tx_hash = self.contract.transact({'from': self.wallet_address}).registerContainer(
            vcType=vc_type, available=available, vcpu=vcpu, vram=vram, vdisk=vdisk, id=id,
            ipAddress=ip_address, controller=controller, created=created
        )

        return tx_hash

    def pending_migrate(self, **kwargs):
        """Marks a vcontainer as pending to migrate. Invoked contract method is `pendingMigrate`.

        Args:
            **kwargs: dict with the following entries:
                {
                    vc_tag: hexstring representation of the vcontainer's vcTag
                    timestamp: int unix timestamp of the pending migration request
                    target: string wallet address of the target DC to migrate to
                    price: int, price for the migration
                    invoice: int, id of the related invoice
                }

        Returns:
            string: transaction hash (tx hash)
        """
        vc_tag = kwargs.pop('vc_tag')
        timestamp = kwargs.pop('timestamp')
        target = kwargs.pop('target')
        price = kwargs.pop('price')
        invoice = kwargs.pop('invoice')

        tx_hash = self.contract.transact({'from': self.wallet_address}).pendingMigrate(
            vcTag=Web3.toBytes(hexstr=vc_tag), timestamp=timestamp, target=target, price=price, invoice=invoice
        )

        return tx_hash

    def migrate_container(self, **kwargs):
        """Confirms a pending to migrate vcontainer to this DC. Invoked contract method is `migrateContainer`.

        Args:
            **kwargs: dict with the following entries:
                {
                    vc_tag: hexstring representation of the vcontainer's vcTag
                    price: int, price for the migration
                    invoice: int, id of the related invoice
                    id: string, the openstack/docker id,
                    ip_address: string, the ip of the container,
                    controller: string, the endpoint of the virtualization controller,
                    timestamp: int unix timestamp of the pending migration request
                }

        Returns:
            string: transaction hash (tx hash)
        """
        vc_tag = kwargs.pop('vc_tag')
        price = kwargs.pop('price')
        invoice = kwargs.pop('invoice')
        id = kwargs.pop('id')
        ip_address = kwargs.pop('ip_address')
        controller = kwargs.pop('controller')
        timestamp = kwargs.pop('timestamp')

        tx_hash = self.contract.transact({'from': self.wallet_address}).migrateContainer(
            vcTag=Web3.toBytes(hexstr=vc_tag), price=price, invoice=invoice, id=id,
            ipAddress=ip_address, controller=controller, timestamp=timestamp
        )

        return tx_hash

    def change_availability(self, **kwargs):
        """Change the availability of the vcontainer with the given vc_tag.
        Invoked contract method is `changeAvailability`.

        Args:
            **kwargs: dict with the following entries:
                {
                    vc_tag: hexstring representation of the vcontainer's vcTag
                    status: boolean, the new availability status
                    timestamp: int, unix timestamp of the availability change
                }

        Returns:
            string: transaction hash (tx hash)
        """
        vc_tag = kwargs.pop('vc_tag')
        status = kwargs.pop('status')
        timestamp = kwargs.pop('timestamp')

        tx_hash = self.contract.transact({'from': self.wallet_address}).changeAvailability(
            vcTag=Web3.toBytes(hexstr=vc_tag), status=status, timestamp=timestamp
        )

        return tx_hash
