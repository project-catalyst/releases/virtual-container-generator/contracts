import arrow
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.management import BaseCommand
from django.utils import timezone

from ws.models import DatacenterClient


class Command(BaseCommand):
    """Use only for development!
    """
    help = 'Pending migrates a vcontainer'

    def add_arguments(self, parser):
        parser.add_argument(
            '-n',
            '--name',
            nargs='?',
            type=str,
            action='store',
            dest='name',
            default='DC1',
            help='Name of the DC (defaults to DC1)'
        )

    def handle(self, *args, **options):
        name = options.get('name')
        dc_client = DatacenterClient.objects.get(name=name)
        layer = get_channel_layer()
        async_to_sync(layer.group_send)(dc_client.group_name, {
            'type': 'chain.message',
            'method': 'pending_migrate',
            'vc_tag': '0xb1e72c1bdd716df277bafa5ba9e661ad25c39f931c0d9977400a71c94c84080a',
            'target': '0x22d491bde2303f2f43325b2108d26f1eaba1e32b',
            'price': 10,
            'invoice': 1,
            'timestamp': arrow.get(timezone.now()).timestamp
        })