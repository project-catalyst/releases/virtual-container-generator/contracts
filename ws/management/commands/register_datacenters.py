from django.core.management import BaseCommand
from django.utils import timezone

from api.utils import wait_for_transaction_mined
from ws.ethereum import CatalystEthereumClient
from ws.models import DatacenterClient

DATACENTERS = [
    ('DC1', '0xffcf8fdee72ac11b5c542428b35eef5769c409f0'),
    ('DC2', '0x22d491bde2303f2f43325b2108d26f1eaba1e32b'),
    ('DC3', '0xe11ba2b4d45eaed5996cd0823791e0c93114882d')
]


class Command(BaseCommand):
    """Use only for development!
    """
    help = 'Register the given datacenters (only for dev!)'

    def handle(self, *args, **options):
        client = CatalystEthereumClient()

        for dc_name, wallet in DATACENTERS:
            self.stdout.write('Registering {} with address {}'.format(dc_name, wallet))
            tx_hash = client.handle_message({
                'method': 'register_datacenter',
                'name': dc_name,
                'wallet': wallet
            })

            self.stdout.write('Waiting for transaction to be mined')
            res = wait_for_transaction_mined(tx_hash)
            self.stdout.write(self.style.SUCCESS('Transaction mined in block={}'.format(res['blockNumber'])))
