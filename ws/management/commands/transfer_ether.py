import math

from django.conf import settings
from django.core.management import BaseCommand
from eth_utils import to_wei
from web3 import Web3, HTTPProvider

from api.utils import transfer_ether_amount


class Command(BaseCommand):
    help = 'Transfers ether from the explorer account to any address that needs it'

    def add_arguments(self, parser):
        parser.add_argument(
            '-t',
            '--target',
            nargs='?',
            type=str,
            action='store',
            dest='address',
            default=settings.WALLET_ADDRESS,
            help='The wallet address to transfer ether to.'
        )

        parser.add_argument(
            '-e',
            '--ether',
            nargs='?',
            type=int,
            action='store',
            dest='ether',
            default=math.pow(10, 20),
            help='Amount of ether to transfer'
        )

    def handle(self, *args, **options):
        address = options.get('address')
        ether = options.get('ether')
        explorer_address = settings.EXPLORER_ADDRESS

        self.stdout.write('Attempting to transfer {} ether to {} from address {}'.format(
            ether, address, explorer_address
        ))

        tx_hash = transfer_ether_amount(explorer_address, address, ether)

        self.stdout.write(self.style.SUCCESS('Created transaction with hash {}'.format(tx_hash)))