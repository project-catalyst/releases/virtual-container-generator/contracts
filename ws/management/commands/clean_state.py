from django.core.management import BaseCommand
from django_ethereum_events.models import Daemon

from ws.models import ChainEvent, DatacenterClient, DatacenterMessageLog


class Command(BaseCommand):
    """Use only for development!
    """
    help = 'Cleans the app state (only for dev!)'

    def handle(self, *args, **options):
        ChainEvent.objects.all().delete()
        self.stdout.write(self.style.SUCCESS('Deleted all ChainEvents...'))

        DatacenterClient.objects.all().delete()
        self.stdout.write(self.style.SUCCESS('Deleted all DatacenterClients ...'))

        DatacenterMessageLog.objects.all().delete()
        self.stdout.write(self.style.SUCCESS('Deleted all DatacenterMessageLogs ...'))

        Daemon.get_solo().delete()
        self.stdout.write(self.style.SUCCESS('Reset block monitoring to 0...'))