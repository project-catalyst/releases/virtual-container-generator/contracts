import arrow
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.management import BaseCommand
from django.utils import timezone

from ws.models import DatacenterClient


class Command(BaseCommand):
    """Use only for development!
    """
    help = 'Creates a dummy vcontainer'

    def add_arguments(self, parser):
        parser.add_argument(
            '-n',
            '--name',
            nargs='?',
            type=str,
            action='store',
            dest='name',
            default='DC1',
            help='Name of the DC (defaults to DC1)'
        )

    def handle(self, *args, **options):
        name = options.get('name')
        dc_client = DatacenterClient.objects.get(name=name)
        layer = get_channel_layer()
        async_to_sync(layer.group_send)(dc_client.group_name, {
            'type': 'chain.message',
            'method': 'register_container',
            'vc_type': 'vm:openstack',
            'available': True,
            'vcpu': 4,
            'vram': 2,
            'vdisk': 20,
            'id': '71d68b44-625e-4aca-81e0-fa02c80da6b3',
            'ip_address': '192.168.1.180',
            'controller': 'http://192.168.1.100:8000',
            'created': arrow.get(timezone.now()).timestamp
        })