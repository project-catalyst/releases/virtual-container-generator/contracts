/* ONLY USE FOR DELEVOPMENT */

const accounts = require('./accounts.js')
const Catalyst = artifacts.require('Catalyst')

let dcsCount = 2
let containerData = [
    "vm:openstack", true, 4, 4, 20, "71d68b44-625e-4aca-81e0-fa02c80da6b3",
    "192.168.1.180", 'http://192.168.1.100:8000', "1525335904"
]

module.exports = function(callback) {
    var instance = undefined;
    Catalyst.deployed().then(function(instance_){
        instance = instance_
        for(var i=0; i < dcsCount; i++){
            let dcName = "DC" + (i+1).toString()
            let account = accounts[i+1]
            console.log('Registering DC ' + dcName + " " + account)
            instance.registerDatacenter(dcName, account, {from: accounts[0]})
        }
    }).then(function(){
        console.log('Registering Vcontainer ' + containerData[5])
        instance.registerContainer(...containerData, {from: accounts[1]})
    }).then(function(){
        console.log('Operation successfull')
    }).catch(function(e){
        console.log(e)
    })
}
