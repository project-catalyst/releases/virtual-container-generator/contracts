#!/bin/bash

echo "Configuration time: $(date)." >> /root/config.log

# Database password check
if [[ -z "${DB_PASSWORD}" ]]; then
    PASSWORD=12345678
    echo "Using default database password..."
else
    PASSWORD="${DB_PASSWORD}"
fi

# Django settings module check
if [[ -z "${DJANGO_SETTINGS_MODULE}" ]]; then
    DJANGO_SETTINGS="vcg_api_server.settings.prod"
    echo "Using default DJANGO_SETTINGS_MODULE..."
else
    DJANGO_SETTINGS="${DJANGO_SETTINGS_MODULE}"
fi

# Create .pgpass file
touch ~/.pgpass
echo "*:*:*:postgres:$PASSWORD" > ~/.pgpass
chmod 600 ~/.pgpass
service postgresql start

# Initiliaze the vcg_api_server docker volume for the postgres database
if [ "$DATABASE_INITIALIZED" = 0 ]; then
    echo "Initializing vcg_api_server database (postgresql)..."

    # Set postgresql encoding to UTF8
    su - postgres -c "psql -c \"UPDATE pg_database SET datistemplate = FALSE WHERE datname = 'template1'\""
    su - postgres -c "psql -c \"DROP DATABASE template1\""
    su - postgres -c "psql -c \"CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING='UNICODE' LC_COLLATE='en_US.UTF8' LC_CTYPE='en_US.UTF8'\""
    su - postgres -c "psql -c \"UPDATE pg_database SET datistemplate = TRUE WHERE datname = 'template1'\""
    su - postgres -c "psql -c \"UPDATE pg_database SET datallowconn = FALSE WHERE datname = 'template1'\""
    su - postgres -c "psql -c \"ALTER USER postgres WITH PASSWORD '$PASSWORD'\""

    # Create database
    su - postgres -c "psql -c \"CREATE DATABASE vcg_api_server WITH encoding='UTF8'\""

fi

# Go to deployment directory
cd /opt/vcg_api_server

# Setup apache
cp config/apache/ports.conf /etc/apache2/ports.conf
cp config/apache/vcg_api_server.conf /etc/apache2/sites-available/vcg_api_server.conf && \
 ln -s /etc/apache2/sites-available/vcg_api_server.conf /etc/apache2/sites-enabled/vcg_api_server.conf

# Redis
cp config/redis/redis.conf /etc/redis/redis.conf

# Celery
cp config/celery/celery-supervisor.conf /etc/supervisor/conf.d/celery-supervisor.conf
cp config/celery/celerybeat-supervisor.conf /etc/supervisor/conf.d/celerybeat-supervisor.conf

# Daphne
cp config/daphne/daphne-supervisor.conf /etc/supervisor/conf.d/daphne-supervisor.conf

# Geth
cp config/geth/geth-supervisor.conf /etc/supervisor/conf.d/geth-supervisor.conf
sed -i "s/bootnodeId/$BOOTNODE_ID/g" /etc/supervisor/conf.d/geth-supervisor.conf
sed -i "s/bootnodeIp/$BOOTNODE_IP/g" /etc/supervisor/conf.d/geth-supervisor.conf

# Configure logging folder
chown -R www-data:www-data /var/log/vcg_api_server && \
chmod -R g+s /var/log/vcg_api_server

# set ACL in log folder
setfacl -Rm d:u:www-data:rwx,d:g:www-data:rwx /var/log/vcg_api_server

# Need redis UP to run migrations
service redis-server start

pip3 install -r requirements.txt
export DJANGO_SETTINGS_MODULE=${DJANGO_SETTINGS}
python3 manage.py migrate
python3 manage.py collectstatic --noinput
python3 manage.py create_admin
chown -R www-data:www-data /opt/vcg_api_server

# Start services
service supervisor start && service supervisor status
service apache2 start && service apache2 status

# Clear cached static entries from redis.
# They are regenerated on every run since the /static/ folder is not stored in a docker volume.
redis-cli -n 4 KEYS "*" | xargs redis-cli -n 4 DEL

echo "Initialization completed."
tail -f /dev/null  # Necessary in order for the container to not stop