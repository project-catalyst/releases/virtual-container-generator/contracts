module.exports = {
    networks: {
      development: {
        host: "127.0.0.1",
        port: 7545,
        network_id: "*"
      },
      staging: {
        host: "192.168.1.49",                               // vcg_api_server ip
        port: 7545,                                         // vcg_api_server geth rcp port
        network_id: "32182",
        from: "0xaf5eef68e74857b8e9997009769ad7b7ff428596", // vcg_api_server address
        gas: 4712388
      },
      production: {
        host: "192.168.1.215",                              // vcg_api_server ip
        port: 7545,                                         // vcg_api_server geth rcp port
        network_id: "32182",
        from: "0xaf5eef68e74857b8e9997009769ad7b7ff428596", // vcg_api_server address
        gas: 4712388
      }
    }

};
