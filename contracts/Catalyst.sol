pragma solidity ^0.4.22;

contract Catalyst {
    // Holds the attributes of the vcontainer
    struct Flavor {
        // Number of CPU cores
        uint8 vcpu;
        // RAM size in GB
        uint8 vram;
        // Disk size in GB
        uint8 vdisk;
    }

    // Details regarding the location of the vcontainer
    struct Information {
        // Docker container id or Openstack VM id
        string id;
        // Ip4 if Ip6 address of the container
        string ipAddress;
        // Wallet address of the datacenter that hosts the container
        address host;
        // Endpoint of the virtualization controller
        string controller;
    }

    // Vcontainer struct
    struct Container {
        // Unique id of the container provided by contract
        bytes32 vcTag;
        // Container type (e.g: vm:openstack, container:docker)
        string vcType;
        // If the container is running or not
        bool available;
        // Flavor details
        Flavor flavor;
        // Information details
        Information info;
        // Wallet address of the VM owner
        address owner;
        // Container creation time (provided from outside)
        uint created;
        // `block.timestamp` of the last container update
        uint updated;
    }

    // Datacenter information
    struct Datacenter {
        string name;
        address wallet;
        bool registered;
    }

    // Details about a pending migration
    struct Migration {
        bytes32 vcTag;
        address from;
        address to;
        uint price;
        uint invoice;
    }

    mapping(bytes32 => Container) public containers;
    mapping(address => Datacenter) public datacenters;
    mapping(bytes32 => Migration) public pendingMigrations;
    address operator;

    event DatacenterRegistered(address wallet, uint timestamp, string name);
    event ContainerRegistered(bytes32 vcTag, uint timestamp, address owner);
    event ContainerAvailabilityChanged(
        bytes32 vcTag,
        uint timestamp,
        bool oldStatus,
        bool newStatus
    );
    event ContainerMigrationPending(
        bytes32 vcTag,
        uint timestamp,
        address from,
        address to
    );
    event ContainerMigrated(
        bytes32 vcTag,
        uint timestamp,
        address from,
        address to,
        uint price,
        uint invoice
    );

    modifier onlyOperator {
        require(
            msg.sender == operator,
            "Only contract operator can call this function"
        );
        _;
    }

    modifier onlyDatacenter {
        require(
            datacenters[msg.sender].registered,
            "Only registered datacenters can call this function"
        );
        _;
    }

    modifier onlyOwner(bytes32 vcTag) {
        require(
            containers[vcTag].owner == msg.sender,
            "Only the container owner can call this function"
        );
        _;
    }

    modifier onlyHost(bytes32 vcTag) {
        require(
            containers[vcTag].info.host == msg.sender,
            "Only the host of the container can call this function"
        );
        _;
    }

    constructor() public {
        operator = msg.sender;
    }

    function getContainer(bytes32 vcTag) public view returns (
        string vcType,
        bool available,
        string id,
        string ipAddress,
        address host,
        string controller,
        address owner,
        uint created,
        uint updated
        ) {

        Container storage container = containers[vcTag];

        return (
            container.vcType,
            container.available,
            container.info.id,
            container.info.ipAddress,
            container.info.host,
            container.info.controller,
            container.owner,
            container.created,
            container.updated
        );
    }

    function getContainerFlavor(bytes32 vcTag) public view returns (
        uint8 vcpu,
        uint8 vram,
        uint8 vdisk
        ) {

        Container storage container = containers[vcTag];

        return (
            container.flavor.vcpu,
            container.flavor.vram,
            container.flavor.vdisk
        );
    }

    function registerDatacenter(string name, address wallet) onlyOperator public{
        datacenters[wallet] = Datacenter({
            name: name,
            wallet: wallet,
            registered: true
        });

        emit DatacenterRegistered(wallet, now, name);
    }

    function registerContainer(
        string vcType,
        bool available,
        uint8 vcpu,
        uint8 vram,
        uint8 vdisk,
        string id,
        string ipAddress,
        string controller,
        uint created
    ) public onlyDatacenter {
        // Generate vcTag
        bytes32 vcTag = keccak256(vcType, id, msg.sender, created);

        Flavor memory flavor = Flavor({
            vcpu: vcpu,
            vram: vram,
            vdisk: vdisk
        });

        Information memory info = Information({
            id: id,
            ipAddress: ipAddress,
            host: msg.sender,
            controller: controller
        });

        // Add the container
        containers[vcTag] = Container({
            vcTag: vcTag,
            vcType: vcType,
            flavor: flavor,
            info: info,
            owner: msg.sender,
            created: created,
            available: available,
            updated: now
        });

        emit ContainerRegistered(vcTag, created, msg.sender);
    }

    function pendingMigrate(
        bytes32 vcTag,
        uint timestamp,
        address target,
        uint price,
        uint invoice
    ) public onlyOwner(vcTag) {
        require(
            datacenters[target].registered,
            "Invalid target datacenter"
        );

        Container storage container = containers[vcTag];
        address host = container.info.host;

        pendingMigrations[vcTag] = Migration({
            vcTag: vcTag,
            from: host,
            to: target,
            price: price,
            invoice: invoice
        });

        emit ContainerMigrationPending(vcTag, timestamp, host, target);
    }

    function migrateContainer(
        bytes32 vcTag,
        uint price,
        uint invoice,
        string id,
        string ipAddress,
        string controller,
        uint timestamp
    ) public onlyDatacenter {
        require(
            pendingMigrations[vcTag].to == msg.sender,
            "Only target Datacenter can migrate the container"
        );

        require(
            (pendingMigrations[vcTag].price == price) &&
            (pendingMigrations[vcTag].invoice == invoice),
            "Migration invoice details are invalid"
        );

        // Update information & ownership
        Container storage container = containers[vcTag];
        address oldHost = container.info.host;
        container.info.id = id;
        container.info.ipAddress = ipAddress;
        container.info.host = msg.sender;
        container.info.controller = controller;
        container.updated = now;


        // Delete pending migration entry
        delete pendingMigrations[vcTag];

        emit ContainerMigrated(vcTag, timestamp, oldHost, msg.sender, price, invoice);
    }

    function changeAvailability(bytes32 vcTag, bool status, uint timestamp)
    public onlyHost(vcTag) {
        Container storage container = containers[vcTag];
        bool oldStatus = container.available;

        if(oldStatus != status){
            container.available = status;
            container.updated = now;

            emit ContainerAvailabilityChanged(vcTag, timestamp, oldStatus, status);
        }
    }
}
