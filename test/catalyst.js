const truffleAssert = require('truffle-assertions');
const Catalyst = artifacts.require('Catalyst')

const vcInfo = [
    "vm:openstack", true, 4, 4, 20, "71d68b44-625e-4aca-81e0-fa02c80da6b3",
    "192.168.1.180", 'http://192.168.1.100:8000', "1525335904"
]

let createdVCTag = undefined;

contract('Catalyst', async(accounts) => {
    async function registerDC(instance, name, wallet, operator) {
        await instance.registerDatacenter(name, wallet, {from: operator})
        let dcInformation = await instance.datacenters.call(wallet)
        return dcInformation
    }

    it('should allow the operator to add a new datacenter', async () => {
        let dcName = "Datacenter 1"
        let instance = await Catalyst.deployed();
        let dcInformation = await registerDC(instance, dcName, accounts[1], accounts[0])
        expect(dcInformation).to.eql([dcName, accounts[1], true])
    })

    it('should not allow anyone else to add a new datacenter', async () => {
        let dcName = "Datacenter 1"
        let instance = await Catalyst.deployed();
        try {
            let dcInformation = await registerDC(instance, dcName, accounts[1], accounts[2])
            assert(false)
        } catch(err) {
            assert(err)
        }
    })

    it('should allow a datacenter to register a VContainer', async () => {
        let dcName = "Datacenter 1"
        let instance = await Catalyst.deployed();
        await registerDC(instance, dcName, accounts[1], accounts[0])
        let result = await instance.registerContainer(...vcInfo, {from: accounts[1]})

        // Verify that the 'ContainerRegistered' event has fired
        truffleAssert.eventEmitted(result, 'ContainerRegistered', (ev) => {
            createdVCTag = ev.vcTag
            return ev.owner === accounts[1]
        });
    })

    it('should not allow a non registered datacenter to register a VContainer', async () => {
        let instance = await Catalyst.deployed();
        try {
            let result = await instance.registerContainer(...vcInfo, {from: accounts[1]})
            assert(false)
        } catch(err) {
            assert(err)
        }
    })

    it('should allow a datacenter to mark a Vcontainer as pending to migrate', async () => {
        const price = 100
        const invoice = 1
        let dcName1 = "Datacenter 1"
        let dcName2 = "Datacenter 2"
        let instance = await Catalyst.deployed();
        await registerDC(instance, dcName1, accounts[1], accounts[0])
        await registerDC(instance, dcName2, accounts[2], accounts[0])
        await instance.registerContainer(...vcInfo, {from: accounts[1]})
        var pendingMigrateData = [
            createdVCTag, 1525335904, accounts[2], price, invoice
        ]

        // Verify that the 'ContainerMigrationPending' has fired
        let result = await instance.pendingMigrate(...pendingMigrateData, {from: accounts[1]})
        truffleAssert.eventEmitted(result, 'ContainerMigrationPending', (ev) => {
            return (ev.vcTag === createdVCTag) && (ev.to === accounts[2])
        });

        // Verify that the pending migration has been saved
        let pendingMigration = await instance.pendingMigrations.call(createdVCTag)
        expect(pendingMigration).to.eql([createdVCTag, accounts[1], accounts[2], web3.toBigNumber(100), web3.toBigNumber(1)])
    })

    it('should not allow a DC to migrate a Vcontainer in a non registered DC', async() => {
        const price = 100
        const invoice = 1
        let dcName1 = "Datacenter 1"
        let instance = await Catalyst.deployed();
        await registerDC(instance, dcName1, accounts[1], accounts[0])
        await instance.registerContainer(...vcInfo, {from: accounts[1]})
        var pendingMigrateData = [
            createdVCTag, 1525335904, accounts[2], price, invoice
        ]
        try {
            let result = await instance.pendingMigrate(...pendingMigrateData, {from: accounts[1]})
        } catch(err) {
            assert(err)
        }
    })

    it('should not allow a DC to migrate a Vcontainer that it doesnt own', async() =>{
        const price = 100
        const invoice = 1
        let dcName1 = "Datacenter 1"
        let dcName2 = "Datacenter 2"
        let dcName3 = "Datacenter 3"
        let instance = await Catalyst.deployed();
        await registerDC(instance, dcName1, accounts[1], accounts[0])
        await registerDC(instance, dcName2, accounts[2], accounts[0])
        await registerDC(instance, dcName3, accounts[3], accounts[0])
        await instance.registerContainer(...vcInfo, {from: accounts[1]})
        var pendingMigrateData = [
            createdVCTag, 1525335904, accounts[3], price, invoice
        ]
        try {
            let result = await instance.pendingMigrate(...pendingMigrateData, {from: accounts[2]})
        } catch(err) {
            assert(err)
        }
    })

    it('should allow the target DC to confirm the migration of a Vcontainer', async () => {
        const price = 100
        const invoice = 1
        let dcName1 = "Datacenter 1"
        let dcName2 = "Datacenter 2"
        let instance = await Catalyst.deployed();
        await registerDC(instance, dcName1, accounts[1], accounts[0])
        await registerDC(instance, dcName2, accounts[2], accounts[0])
        await instance.registerContainer(...vcInfo, {from: accounts[1]})

        var pendingMigrateData = [createdVCTag, 1525335904, accounts[2], price, invoice]
        await instance.pendingMigrate(...pendingMigrateData, {from: accounts[1]})

        var confirmMigrateData = [
            createdVCTag, price, invoice, "aa468b44-625e-4aca-81c0-fa02c80da6b3",
            "83.1.1.1", "http://83.1.1.10:555", 1525336304
        ]

        // Verify that the ContainerMigrated event has fired
        let result = await instance.migrateContainer(...confirmMigrateData, {from: accounts[2]})
        truffleAssert.eventEmitted(result, 'ContainerMigrated', (ev) => {
            return expect(ev).to.eql({
                vcTag: createdVCTag,
                timestamp: web3.toBigNumber(1525336304),
                from: accounts[1],
                to: accounts[2],
                price: web3.toBigNumber(100),
                invoice: web3.toBigNumber(1)
            })
        });

        // Verify the new state of the container
        let container = await instance.getContainer.call(createdVCTag)
        container.splice(-1,1) // remove the 'updated' attribute since it is dynamic
        expect(container).to.eql([
            "vm:openstack", true, "aa468b44-625e-4aca-81c0-fa02c80da6b3",
            "83.1.1.1", accounts[2], "http://83.1.1.10:555", accounts[1], web3.toBigNumber(1525335904)
        ])
    })

    it('should not allow the target DC to confirm a migration with wrong price', async () => {
        const price = 100
        const invoice = 1
        let dcName1 = "Datacenter 1"
        let dcName2 = "Datacenter 2"
        let instance = await Catalyst.deployed();
        await registerDC(instance, dcName1, accounts[1], accounts[0])
        await registerDC(instance, dcName2, accounts[2], accounts[0])
        await instance.registerContainer(...vcInfo, {from: accounts[1]})

        var pendingMigrateData = [createdVCTag, 1525335904, accounts[2], price, invoice]
        await instance.pendingMigrate(...pendingMigrateData, {from: accounts[1]})

        var confirmMigrateData = [
            createdVCTag, price-1, invoice, "aa468b44-625e-4aca-81c0-fa02c80da6b3",
            "83.1.1.1", "http://83.1.1.10:555", 1525336304
        ]

        try {
            await instance.migrateContainer(...confirmMigrateData, {from: accounts[2]})
            assert(false)
        } catch(err) {
            assert(err)
        }
    })

    it('should not allow the target DC to confirm a migration with wrong invoice id', async () => {
        const price = 100
        const invoice = 1
        let dcName1 = "Datacenter 1"
        let dcName2 = "Datacenter 2"
        let instance = await Catalyst.deployed();
        await registerDC(instance, dcName1, accounts[1], accounts[0])
        await registerDC(instance, dcName2, accounts[2], accounts[0])
        await instance.registerContainer(...vcInfo, {from: accounts[1]})

        var pendingMigrateData = [createdVCTag, 1525335904, accounts[2], price, invoice]
        await instance.pendingMigrate(...pendingMigrateData, {from: accounts[1]})

        var confirmMigrateData = [
            createdVCTag, price, invoice+1, "aa468b44-625e-4aca-81c0-fa02c80da6b3",
            "83.1.1.1", "http://83.1.1.10:555", 1525336304
        ]

        try {
            await instance.migrateContainer(...confirmMigrateData, {from: accounts[2]})
            assert(false)
        } catch(err) {
            assert(err)
        }
    })

    it('should not allow a DC to confirm a migration not meant for him', async () => {
        const price = 100
        const invoice = 1
        let dcName1 = "Datacenter 1"
        let dcName2 = "Datacenter 2"
        let dcName3 = "Datacenter 3"
        let instance = await Catalyst.deployed();
        await registerDC(instance, dcName1, accounts[1], accounts[0])
        await registerDC(instance, dcName2, accounts[2], accounts[0])
        await registerDC(instance, dcName3, accounts[3], accounts[0])

        var pendingMigrateData = [createdVCTag, 1525335904, accounts[2], price, invoice]
        await instance.pendingMigrate(...pendingMigrateData, {from: accounts[1]})

        var confirmMigrateData = [
            createdVCTag, price, invoice, "aa468b44-625e-4aca-81c0-fa02c80da6b3",
            "83.1.1.1", "http://83.1.1.10:555", 1525336304
        ]

        try {
            await instance.migrateContainer(...confirmMigrateData, {from: accounts[3]})
            assert(false)
        } catch(err) {
            assert(err)
        }
    })

    it('should allow a host of a Vcontainer to change the availability of the container', async () => {
        let dcName1 = "Datacenter 1"
        let instance = await Catalyst.deployed();
        await registerDC(instance, dcName1, accounts[1], accounts[0])
        await instance.registerContainer(...vcInfo, {from: accounts[1]})
        let changeAvailabilityData = [createdVCTag, false, 1525336304]
        let result = await instance.changeAvailability(...changeAvailabilityData, {from: accounts[1]})

        // Verify that the 'ContainerAvailabilityChanged' has fired
        truffleAssert.eventEmitted(result, 'ContainerAvailabilityChanged', (ev) => {
            return expect(ev).to.eql({
                vcTag: createdVCTag,
                timestamp: web3.toBigNumber(1525336304),
                oldStatus: true,
                newStatus: false
            })
        });

        // Verify that the status change has persisted
        let container = await instance.getContainer.call(createdVCTag)
        assert.equal(container[1], false)
    })

    it('should not allow a DC to change the availability of a VM it does not host', async () => {
        let dcName1 = "Datacenter 1"
        let dcName2 = "Datacenter 2"
        let instance = await Catalyst.deployed();
        await registerDC(instance, dcName1, accounts[1], accounts[0])
        await registerDC(instance, dcName2, accounts[2], accounts[0])
        await instance.registerContainer(...vcInfo, {from: accounts[1]})

        let changeAvailabilityData = [createdVCTag, false, 1525336304]
        try {
            await instance.changeAvailability(...changeAvailabilityData, {from: accounts[2]})
            assert(false)
        } catch(err){
            assert(err)
        }
    })
})
